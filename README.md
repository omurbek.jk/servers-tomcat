Tomcat Server
=============


## Description
This composite creates a simple Tomcat server cluster via an Autoscaling Group



## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/servers-tomcat/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

### `TOMCAT_KEYPAIR`:
  * description: 


## Required variables with default

### `TOMCAT_GROUP_MAXIMUM`:
  * description: Minimum number of instances the autoscale group will launch
  * default: 1

### `TOMCAT_GROUP_MINIMUM`:
  * description: Maximum number of instances the autoscale group will launch
  * default: 1

### `TOMCAT_INGRESS_CIDRS`:
  * description: 

### `TOMCAT_INGRESS_CIDR_PORTS`:
  * description: 
  * default: 1f90, 50, 1bb, 16

### `TOMCAT_INGRESS_GROUP_PORTS`:
  * description: 
  * default: 1f90, 50, 1bb

### `TOMCAT_NAME`:
  * description: 
  * default: tomcat


### `TOMCAT_SIZE`:
  * description: 
  * default: t2.micro


### `VPC_NAME`:
  * description: 
  * default: test-vpc

### `VPC_CIDR`:
  * description: The CIDR to match to locate the VPC that this server is to be created in
  * default: 10.11.0.0/16

### `PUBLIC_ROUTE_NAME`:
  * description: 
  * default: test-public-route

### `PUBLIC_SUBNET_NAME`:
  * description: The name of the public subnet you want to create your server in
  * default: test-public-subnet


## Optional variables with default

**None**


## Optional variables with no default

### `YUM_REPO_BUCKET`:
  * description: 

## Tags
1. Servers
1. Tomcat


## Categories
1. Servers



## Diagram
![diagram](https://raw.githubusercontent.com/CloudCoreo/servers-tomcat/master/images/diagram.png "diagram")


## Icon
![icon](https://raw.githubusercontent.com/CloudCoreo/servers-tomcat/master/images/tomcat.png "icon")

